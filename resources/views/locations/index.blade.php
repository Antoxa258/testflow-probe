<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.dark\:text-gray-500{--tw-text-opacity:1;color:#6b7280;color:rgba(107,114,128,var(--tw-text-opacity))}}
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
            @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                        <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                <div class="flex justify-center pt-8 sm:justify-center sm:pt-0">
                    <svg xmlns="http://www.w3.org/2000/svg" width="188.061" height="40.091" viewBox="0 0 188.061 40.091" class="h-8"><g id="Gruppe_32" data-name="Gruppe 32" transform="translate(-3.472 -1.727)"><g id="Gruppe_1" data-name="Gruppe 1" transform="translate(60.713 9.398)"><path id="Pfad_1" data-name="Pfad 1" d="M57.342,14.391v4.1H46.305v-4.1ZM48.618,10.2h5.569V26.385a1.872,1.872,0,0,0,.159.831,1,1,0,0,0,.49.444,2.069,2.069,0,0,0,.763.126,4.288,4.288,0,0,0,.672-.056c.243-.047.425-.084.547-.115l.843,4.021c-.266.076-.643.171-1.128.285a9.422,9.422,0,0,1-1.72.216,8.185,8.185,0,0,1-3.36-.456,4.35,4.35,0,0,1-2.13-1.732A5.351,5.351,0,0,1,48.618,27ZM68.173,32.216a9.786,9.786,0,0,1-4.737-1.082,7.482,7.482,0,0,1-3.052-3.11,10.264,10.264,0,0,1-1.06-4.818,10,10,0,0,1,1.07-4.737A7.726,7.726,0,0,1,63.413,15.3,8.928,8.928,0,0,1,68,14.164a9.657,9.657,0,0,1,3.417.581,7.32,7.32,0,0,1,2.677,1.72,7.826,7.826,0,0,1,1.753,2.813,10.759,10.759,0,0,1,.627,3.815v1.39H61.271V21.248H71.294a3.125,3.125,0,0,0-.421-1.584,2.869,2.869,0,0,0-1.128-1.081,3.252,3.252,0,0,0-1.629-.4,3.325,3.325,0,0,0-1.673.422,3.089,3.089,0,0,0-1.173,1.128,3.158,3.158,0,0,0-.456,1.6V24.63a4.021,4.021,0,0,0,.421,1.88,3.049,3.049,0,0,0,1.2,1.241,3.656,3.656,0,0,0,1.846.444,4.166,4.166,0,0,0,1.343-.2,2.876,2.876,0,0,0,1.037-.6,2.491,2.491,0,0,0,.649-.98l5.114.148a6.332,6.332,0,0,1-1.4,2.995,7.106,7.106,0,0,1-2.813,1.971A10.848,10.848,0,0,1,68.173,32.216ZM94.594,19.733l-5.114.136a1.917,1.917,0,0,0-.433-.969,2.319,2.319,0,0,0-.922-.671,3.146,3.146,0,0,0-1.3-.251,3.3,3.3,0,0,0-1.652.388,1.146,1.146,0,0,0-.66,1.048,1.119,1.119,0,0,0,.41.889,3.5,3.5,0,0,0,1.537.6l3.372.639a7.934,7.934,0,0,1,3.9,1.662,3.98,3.98,0,0,1,1.3,3.075,4.936,4.936,0,0,1-1.072,3.132,6.8,6.8,0,0,1-2.869,2.073,11.148,11.148,0,0,1-4.169.73,10.041,10.041,0,0,1-5.911-1.538,5.745,5.745,0,0,1-2.46-4.145l5.5-.138a2.141,2.141,0,0,0,.945,1.458,3.485,3.485,0,0,0,1.947.5,3.378,3.378,0,0,0,1.743-.4,1.224,1.224,0,0,0,.684-1.06,1.126,1.126,0,0,0-.524-.945,4.17,4.17,0,0,0-1.606-.569L84.184,24.8a7.241,7.241,0,0,1-3.918-1.765,4.454,4.454,0,0,1-1.275-3.3,4.749,4.749,0,0,1,.933-3,5.872,5.872,0,0,1,2.689-1.914,11.6,11.6,0,0,1,4.111-.66,9.355,9.355,0,0,1,5.6,1.491A5.345,5.345,0,0,1,94.594,19.733Zm13.05-5.343v4.1H96.607v-4.1ZM98.919,10.2h5.57V26.385a1.872,1.872,0,0,0,.159.831,1,1,0,0,0,.49.444,2.069,2.069,0,0,0,.763.126,4.272,4.272,0,0,0,.671-.056c.243-.047.426-.084.548-.115l.842,4.021q-.4.114-1.128.285a9.391,9.391,0,0,1-1.719.216,8.188,8.188,0,0,1-3.361-.456,4.346,4.346,0,0,1-2.129-1.732A5.362,5.362,0,0,1,98.919,27Zm21.509,4.191v4.1H109.176v-4.1Zm-8.906,17.494V13.5a6.685,6.685,0,0,1,.763-3.361,4.823,4.823,0,0,1,2.118-2,6.966,6.966,0,0,1,3.132-.671,12.478,12.478,0,0,1,2.186.182q1.036.17,1.538.307l-.889,4.077a4.669,4.669,0,0,0-.752-.171,5.274,5.274,0,0,0-.819-.068,1.7,1.7,0,0,0-1.344.433,1.779,1.779,0,0,0-.365,1.184V31.885ZM128.677,8.56V31.885h-5.57V8.56Zm11.711,23.656a9.436,9.436,0,0,1-4.737-1.128,7.784,7.784,0,0,1-3.042-3.167A10.114,10.114,0,0,1,131.55,23.2a10.07,10.07,0,0,1,1.058-4.727A7.7,7.7,0,0,1,135.65,15.3a10.4,10.4,0,0,1,9.464,0,7.588,7.588,0,0,1,3.042,3.167,9.977,9.977,0,0,1,1.07,4.727,10.021,10.021,0,0,1-1.07,4.725,7.675,7.675,0,0,1-3.042,3.167A9.373,9.373,0,0,1,140.388,32.216Zm.035-4.2a2.47,2.47,0,0,0,1.7-.616,3.852,3.852,0,0,0,1.06-1.708,7.787,7.787,0,0,0,.376-2.528,7.867,7.867,0,0,0-.376-2.551,3.856,3.856,0,0,0-1.06-1.709,2.476,2.476,0,0,0-1.7-.615,2.6,2.6,0,0,0-1.753.615,3.863,3.863,0,0,0-1.082,1.709,8.067,8.067,0,0,0-.365,2.551,7.984,7.984,0,0,0,.365,2.528,3.859,3.859,0,0,0,1.082,1.708A2.6,2.6,0,0,0,140.422,28.014Zm14.55,3.871-4.556-17.494h5.591L158.367,25.6h.147l2.528-11.208H166.5l2.6,11.128h.148l2.289-11.128h5.593l-4.568,17.494H166.6l-2.734-10.192h-.194l-2.733,10.192Z" transform="translate(-46.305 -7.467)" fill="#4338ca"></path></g> <g id="Gruppe_2" data-name="Gruppe 2" transform="translate(3.472 1.727)"><path id="Pfad_2" data-name="Pfad 2" d="M20.061,38.393a5.376,5.376,0,0,0,.365-2.1,12.024,12.024,0,0,1,5.589-9.925,5.207,5.207,0,0,0,0-9.185,15.519,15.519,0,0,1-1.368-1.066l-1.092-1.069a11.392,11.392,0,0,1-3.136-7.794,5.368,5.368,0,0,0-1.514-3.9,5.341,5.341,0,0,0-7.675,0,5.4,5.4,0,0,0-1.514,3.9,5.3,5.3,0,0,0,2.707,4.561,12.257,12.257,0,0,1,5.573,9.965,11.351,11.351,0,0,1-3.079,7.742L12.43,31.73a5.3,5.3,0,0,0-2.709,4.562,5.35,5.35,0,0,0,10.339,2.1Z" transform="translate(-1.372 -1.727)" fill="#a5b4fc"></path> <path id="Pfad_3" data-name="Pfad 3" d="M23.115,10.193A5.392,5.392,0,0,1,23.782,3.4a5.345,5.345,0,0,1,8.732,1.744,5.358,5.358,0,0,1-9.4,5.047Z" transform="translate(2.832 -1.694)" fill="#a5b4fc"></path> <path id="Pfad_4" data-name="Pfad 4" d="M24.592,24.474a5.341,5.341,0,0,1,2.975-.906v0a5.369,5.369,0,0,1,5.328,4.854,5.382,5.382,0,0,1-3.279,5.5,5.333,5.333,0,0,1-5.835-1.167,5.391,5.391,0,0,1,.811-8.276Z" transform="translate(2.832 5.618)" fill="#a5b4fc"></path> <path id="Pfad_5" data-name="Pfad 5" d="M38.264,15.087a5.375,5.375,0,0,1-4.453,8.367,5.355,5.355,0,0,1-4.947-3.32,5.4,5.4,0,0,1,1.161-5.863,5.34,5.34,0,0,1,8.239.815Z" transform="translate(4.933 1.962)" fill="#a5b4fc"></path> <path id="Pfad_6" data-name="Pfad 6" d="M5.851,13.6a5.357,5.357,0,0,1,8.3,3.948A5.352,5.352,0,0,1,5.04,21.878,5.392,5.392,0,0,1,5.851,13.6Z" transform="translate(-3.472 1.963)" fill="#a5b4fc"></path></g></g></svg> 
                
                </div>

                <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                    <div class="grid grid-cols-1 md:grid-cols-2">
                        <div class="p-6">
                            <div class="flex items-center">
                                <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-500"><path d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253"></path></svg>
                                <div class="ml-4 text-lg leading-7 font-semibold"><h1>Company: {{$company->name}}</h1></div>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                    Laravel has wonderful, thorough documentation covering every aspect of the framework. Whether you are new to the framework or have previous experience with Laravel, we recommend reading all of the documentation from beginning to end.
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
                            <div class="flex items-center">
                                <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-500"><path d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path><path d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path></svg>
                                <div class="ml-4 text-lg leading-7 font-semibold">Locations:</div>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                    <ul>
                                    @foreach($company->locations as $location)
                                        <li>{{ $location->name }}</li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700">
                            <div class="flex items-center">
                                <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-500"><path d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z"></path></svg>
                                <div class="ml-4 text-lg leading-7 font-semibold">Nutzer:</div>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                    <ul>
                                    @foreach($company->locations[0]->users as $user)
                                        <li>{{ $user->first_name }} {{ $user->last_name }}</li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-l">
                            <div class="flex items-center">
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </body>
</html>
