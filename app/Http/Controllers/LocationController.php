<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Company;

class LocationController extends Controller
{
    public function index()
    {
        $company = Company::first();

        return view('locations.index', compact('company'));
    }
}
